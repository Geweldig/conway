use rand::Rng;
use std::io::{stdout, Write};
use std::{thread, time};
use terminal_size::{terminal_size, Height, Width};
use termion::raw::IntoRawMode;

struct Board {
    genomes: Vec<Vec<bool>>,
    width: usize,
    height: usize,
}

impl Board {
    fn new_auto() -> Board {
        let (Width(width), Height(height)) = terminal_size()
            .unwrap_or_else(|| panic!("Failed to get terminal size. Not running in a tty?"));
        Self::new(width.into(), (height - 1).into())
    }

    fn new(width: usize, height: usize) -> Board {
        Board {
            genomes: vec![vec![false; width]; height],
            width,
            height,
        }
    }

    fn init(&mut self) {
        let mut rng = rand::thread_rng();
        for y in 0..self.height {
            for x in 0..self.width {
                self.genomes[y][x] = rng.gen::<f64>() > 0.7;
            }
        }
    }

    fn draw(&self) {
        let mut output = String::from("");
        for y in 0..self.height {
            for x in 0..self.width {
                if self.genomes[y][x] {
                    output.push('O');
                } else {
                    output.push(' ');
                }
            }
            output.push('\r');
            output.push('\n');
        }
        let mut stdout = stdout().into_raw_mode().unwrap();
        write!(stdout, "{}", output).unwrap();
        stdout.flush().unwrap();
    }

    fn mutate(&mut self) {
        let mut new_self = Self::new(self.width, self.height);
        for y in 0..self.height {
            for x in 0..self.width {
                let neighbours = vec![
                    (x as isize - 1, y as isize + 1),
                    (x as isize, y as isize + 1),
                    (x as isize + 1, y as isize + 1),
                    (x as isize + 1, y as isize),
                    (x as isize + 1, y as isize - 1),
                    (x as isize, y as isize - 1),
                    (x as isize - 1, y as isize - 1),
                    (x as isize - 1, y as isize),
                ]
                .iter()
                .map(|(nx, ny)| {
                    if *nx > 0 && *nx < self.width as isize && *ny > 0 && *ny < self.height as isize
                    {
                        self.genomes[*ny as usize][*nx as usize]
                    } else {
                        false
                    }
                })
                .filter(|i| *i)
                .count();
                new_self.genomes[y][x] = matches!(
                    (neighbours, self.genomes[y][x]),
                    (2, true) | (3, true) | (3, false)
                );
            }
        }
        *self = new_self;
    }
}

fn main() {
    let mut board = Board::new_auto();
    board.init();
    for _ in 0..200 {
        board.draw();
        board.mutate();
        thread::sleep(time::Duration::from_millis(150));
    }
}
